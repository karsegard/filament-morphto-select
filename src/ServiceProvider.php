<?php
namespace KDA\Filament\MorphToSelect;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
//use Illuminate\Support\Facades\Blade;
    use KDA\Laravel\Traits\HasProviders;
class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasProviders;
    protected $packageName ='filament-morphto-select';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
        //register filament provider
    protected $additionnalProviders=[
        \KDA\Filament\MorphToSelect\FilamentServiceProvider::class
    ];
    public function register()
    {
        parent::register();
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
